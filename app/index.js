const express = require('express')
const helmet = require('helmet')
const lusca = require('lusca')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')

const endpoints = require('./endpoints')

const config = require('./config')
const app = express()

//parse POST
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(morgan(config.env))

// security middlewares
app.use(lusca.xframe('SAMEORIGIN'))
app.use(lusca.xssProtection(true))

app.use(cors())

app.use('/', endpoints)

app.use(helmet())

module.exports = app
