const jwt = require('jsonwebtoken')
const apolloAuth = require('@apollo/apollo-auth-lib')

const User = require('../models/User')
const logger = require('../helpers/logger')
const config = require('../config')

exports.authenticate = (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) return res.json({success: false, message: 'username and password is required'})

  User.findOne({ username }, (err, user) => {
    if (err) throw err

    if (!user) return res.status(404).json({ success: false, message: 'User not found' })

    if (!apolloAuth.validateHash(user.password, password)) {
      return res.status(401).json({ message: 'Invalid password' })
    }

    const token = jwt.sign(user, config.secret, { expiresIn: config.tokenExpiration })

    return res.json({
      success: true,
      message: 'successfully authenticated',
      token
    })
  })
}

exports.verify = (req, res) => {
  const token = req.body.token || req.query.token || req.headers['Authorization'];

  if (!token) return res.status(403).send({ success: false, massage: 'no token provided' });

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) return res.json({ success: false, message: 'Failed to authenticate token' });

    return res.json({ success: true, token })
  });

}