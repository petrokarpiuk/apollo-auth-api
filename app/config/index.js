const path = require('path')

module.exports = {
  port: 8080,
  env: process.env.ENV || 'dev',
  mongo: {
    host: 'ds137729.mlab.com',
    port: 37729,
    database: 'apollo',
    username: 'apollo_admin',
    password: 'apollo_admin'
  },
  logger: {
    logFile: 'app.log',
    folder: (path.join(__filename, '../../../logs'))
  },
  'secret': 'secrettoken',
  'tokenExpiration': Math.floor(Date.now() / 1000) + (60 * 60)
}
