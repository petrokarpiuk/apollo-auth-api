const router = require('express').Router()

router.use('/auth', require('./auth'))
router.use('/', (req, res) => res.end('Auth API is running'))

module.exports = router
