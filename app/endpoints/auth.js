const router = require('express').Router()
const ctrl = require('../controllers/authController')

router.post('/', ctrl.authenticate)
router.post('/verify', ctrl.verify)

module.exports = router
