const mongoose = require('mongoose')
const { Schema } = mongoose

const User = mongoose.Schema({
  email: String,
  name: String,
  firstName: String,
  lastName: String,
  password: String,
  admin: Boolean
})

module.exports = mongoose.model('User', User)
