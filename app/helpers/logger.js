const fs = require('fs')
const path = require('path')

const winston = require('winston')
const config = require('../config').logger

const logFile = path.join(config.folder, config.logFile)

if (!fs.existsSync(logFile)) {
  if (!fs.existsSync(config.folder)) {
    fs.mkdirSync(config.folder)
  }

  fs.writeFileSync(logFile, '')
}

const logger = new winston.Logger({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({ filename: logFile })
  ]
})

module.exports = logger
