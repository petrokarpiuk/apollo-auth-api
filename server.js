const mongoose = require('mongoose')
const config = require('./app/config')

const app = require('./app')

const logger = require('./app/helpers/logger')

mongoose.connect(`mongodb://${config.mongo.username}:${config.mongo.password}@${config.mongo.host}:${config.mongo.port}/${config.mongo.database}`)

const db = mongoose.connection

db.on('error', err => logger.error(err))
db.once('open', () => console.log('Connected'))

const port = process.env.PORT || config.port

app.listen(port, () => console.log(`App is running on port: ${port}`))
